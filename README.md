# GeoOp PHP SDK for APIv3 #
Note: API is currently internal-only; SDK for public review only.

## Installation ##

This library can be managed via composer.

**composer.json**

```
#!json

{
  "require": {
    "geoop/php-sdk": "0.*"
  }
}
```

Then install the package with *composer.phar* from the same directory as *composer.json* from your terminal

```
#!

$> composer.phar update geoop/php-sdk
```

# In this Library #

## Geoop\Core\ ##

### ApiContext ###

This contains the connection information for the repositories.

* companyId - The target GeoOp company for the API calls

* scope - API scope: ["system", "app", "public"]

* token - API OAuth2 token for the Authorization header.

* url - The base URI for the API calls: ["https://api.dev.geoop.com", "https://api.test.geoop.com", "https://api.geoop.com"]

* version - API version ["v3.0"]

### ApiParser ###

The class that contains the necessary functions to convert data between JSON and models. This class also handles any differences between model/attribute naming between API and SDK.

### CallHandler ###

Deprecated.

### GeoOpConnection ###

Deprecated.

## Geoop\Definitions\ ##

This directory holds all enumerable values and some complex enumerable arrays, as well as most static nouns.

## Geoop\Interfaces\ ##

The interfaces for classes that are expected to be accessed directly by SDK consumers.

## Geoop\Model\ ##

The GeoOp models. While some models may not have their own endpoint, all classes within this directory are instantiable.

## Geoop\ModelService\ ##

Supplementary classes that aid the models with basic functionality and common properties. These are not instantiable.

## Geoop\Repository\ ##

Herein contains all the classes that aid communication with the API. Only models with an associated API endpoint will have a repository. Models without a repository generally must belong to a parent model, and cannot exist on their own. These models may be modified via their parent-class repository.

# Usage #

## Retrieve a Company ##

```
#!php

$apiContext = new ApiContext(
    'https://api.geoop.com',
    'system',
    'v3.0',
    <companyId>,
    <apiToken>
);

$companyRepository = new CompanyRepository($apiContext);

$company = $companyRepository->fetch();

```
