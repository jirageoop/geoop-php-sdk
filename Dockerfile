ARG PHP_VERSION=5.6

FROM php:${PHP_VERSION}-cli AS base-env

# Debian stretch seems to be archived. I did not find an official note yet, but strech is already listed at http://archive.debian.org/debian/dists/stretch/
# We are making use of http://snapshot.debian.org/ if that persists. If we can, we might change to archive.debian.org, which is failling currently
RUN sed -i \
    -e 's/deb.debian.org\/debian/snapshot.debian.org\/archive\/debian\/20230421T152012Z/' \
    -e 's/security.debian.org\/debian-security/snapshot.debian.org\/archive\/debian-security\/20230421T195345Z/' \
    /etc/apt/sources.list

RUN apt-get update \
    && apt-get install -y \
      git unzip \
    && rm -rf /var/lib/apt/lists/*

RUN curl -s https://getcomposer.org/installer | php && \
  mv composer.phar /usr/local/bin/composer

WORKDIR /pkg

COPY composer.json composer.lock /pkg/

RUN composer install --no-interaction

COPY . /pkg
