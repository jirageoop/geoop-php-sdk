<?php
/**
 * @author: Kieran <kieran.m@geoop.com>
 * Date: 14-Dec-15
 */

namespace Geoop\UnitTest;

use Geoop\Model\Account;
use Geoop\Model\AssetAccount;
use Geoop\Model\Company;
use Geoop\Model\ExternalInfo;
use Geoop\Model\IncomeAccount;
use Geoop\Model\PurchaseAccount;
use Geoop\Model\SaleAccount;

abstract class TestData
{
    /**
     * @return Company
     */
    public static function getCompany()
    {
        $company = new Company(true);
        $company->id = 10;
        $company->companyName = 'TestCompany';
        $company->timezone = 'Pacific/Auckland';
        $company->assetAccount = self::getAssetAccount();
        $company->incomeAccount = self::getIncomeAccount();
        $company->purchaseAccountInventory = self::getPurchaseAccount();
        $company->saleAccountInventory = self::getSaleAccount();
        $company->purchaseAccountLabour = self::getPurchaseAccount();
        $company->saleAccountLabour = self::getSaleAccount();

        return $company;
    }

    /**
     * @return Account
     */
    public static function getAccount()
    {
        $account = new Account();
        $account->id = 500;
        $account->active = true;
        $account->code = 'TestAccount01';
        $account->name = 'Test Account';
        $account->payable = true;
        $account->description = 'This is a test account';
        $account->rate = 15.0;
        $account->mode = 'flat';
        $account->externalInfo = new ExternalInfo();
        $account->externalInfo->myob = 'ABC123';

        return $account;
    }

    /**
     * @return AssetAccount
     */
    public static function getAssetAccount()
    {
        $account = new AssetAccount();
        $account->id = 501;
        $account->active = true;
        $account->code = 'TestAssetAccount01';
        $account->name = 'Test Asset Account';
        $account->payable = true;
        $account->description = 'This is a test asset account';
        $account->rate = 10.0;
        $account->mode = 'flat';
        $account->externalInfo = new ExternalInfo();
        $account->externalInfo->myob = 'DEF456';

        return $account;
    }

    /**
     * @return PurchaseAccount
     */
    public static function getPurchaseAccount()
    {
        $account = new PurchaseAccount();
        $account->id = 502;
        $account->active = true;
        $account->code = 'TestPurchaseAccount01';
        $account->name = 'Test Purchase Account';
        $account->payable = true;
        $account->description = 'This is a test purchase account';
        $account->rate = 5.0;
        $account->mode = 'flat';
        $account->externalInfo = new ExternalInfo();
        $account->externalInfo->myob = 'GHI789';

        return $account;
    }

    /**
     * @return SaleAccount
     */
    public static function getSaleAccount()
    {
        $account = new SaleAccount();
        $account->id = 503;
        $account->active = true;
        $account->code = 'TestSaleAccount01';
        $account->name = 'Test Sale Account';
        $account->payable = true;
        $account->description = 'This is a test sale account';
        $account->rate = 10.5;
        $account->mode = 'flat';
        $account->externalInfo = new ExternalInfo();
        $account->externalInfo->myob = 'JKL012';

        return $account;
    }

    /**
     * @return IncomeAccount
     */
    public static function getIncomeAccount()
    {
        $account = new IncomeAccount();
        $account->id = 504;
        $account->active = true;
        $account->code = 'TestIncomeAccount01';
        $account->name = 'Test Income Account';
        $account->payable = true;
        $account->description = 'This is a test income account';
        $account->rate = 5.5;
        $account->mode = 'flat';
        $account->externalInfo = new ExternalInfo();
        $account->externalInfo->myob = 'MNO345';

        return $account;
    }
}