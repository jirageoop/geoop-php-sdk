<?php
/**
 * @author: Kieran <kieran.m@geoop.com>
 * Date: 14-Dec-15
 */

namespace Geoop\UnitTest\Core;

use Geoop\Core\ApiParser;
use Geoop\Model\Request;
use Geoop\UnitTest\TestData;

class ApiParserTest extends \PHPUnit_Framework_TestCase
{
    public function testToJson()
    {
        $request = new Request();
        $company = TestData::getCompany();
        $request->Company = [$company];

        $expectedRequest = new Request();
        $expectedCompany = TestData::getCompany();
        $expectedCompany->taxCodeAsset = $expectedCompany->assetAccount;
        $expectedCompany->taxCodePayment = $expectedCompany->incomeAccount;
        $expectedCompany->taxCodeCostInventory = $expectedCompany->purchaseAccountInventory;
        $expectedCompany->taxCodePriceInventory = $expectedCompany->saleAccountInventory;
        $expectedCompany->taxCodeCostLabour = $expectedCompany->purchaseAccountLabour;
        $expectedCompany->taxCodePriceLabour = $expectedCompany->saleAccountLabour;
        unset($expectedCompany->assetAccount);
        unset($expectedCompany->incomeAccount);
        unset($expectedCompany->purchaseAccountInventory);
        unset($expectedCompany->purchaseAccountLabour);
        unset($expectedCompany->saleAccountInventory);
        unset($expectedCompany->saleAccountLabour);
        $expectedRequest->Account = [$expectedCompany];

        $json = ApiParser::toJSON($request);
        $expectedJson = json_encode($expectedRequest);

        $this->assertEquals($expectedJson, $json);
    }
}