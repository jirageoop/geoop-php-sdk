<?php

// ANY notice/warning/etc. should fail during unit tests.
ini_set('display_errors', 1);
error_reporting(E_ALL);

$rootDir = dirname(__DIR__);
$vendorDir = "$rootDir/vendor";
$autoloader = require("$vendorDir/autoload.php");
