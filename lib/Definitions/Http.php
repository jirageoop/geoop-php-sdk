<?php
/**
 * Created by PhpStorm.
 * User: Kieran
 * Date: 25/03/2015
 * Time: 9:11 AM
 */

namespace Geoop\Definitions;

class Http
{
    const CREATE = 'POST';
    const DELETE = 'DELETE';
    const FETCH = 'GET';
    const UPDATE = 'PATCH';
    const UPSERT = 'PUT';
}
