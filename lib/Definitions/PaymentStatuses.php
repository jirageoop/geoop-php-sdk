<?php
/**
 * @author: Kieran <kieran.m@geoop.com>
 * Date: 21-Nov-15
 */

namespace Geoop\Definitions;

class PaymentStatuses
{
    const FAIL_STATUS = 'failed';
    const NEW_STATUS = 'new';
    const PAID_STATUS = 'paid';
    const PENDING_STATUS = 'pending';
}
