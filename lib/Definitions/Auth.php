<?php
/**
 * Created by PhpStorm.
 * User: Kieran
 * Date: 23/03/2015
 * Time: 7:58 AM
 */

namespace Geoop\Definitions;

class Auth
{
    const CONSUMER_KEY = 'AccessToken';
    const CONSUMER_SECRET = 'AccessSecret';
}
