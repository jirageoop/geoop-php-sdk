<?php
/**
 * @author: Kieran <kieran.m@geoop.com>
 * Date: 18-Nov-15
 */

namespace Geoop\Definitions;

class PaymentTypes
{
    const AMEX = 'AMEX';
    const BANK_TRANSFER = 'Bank Transfer';
    const CASH = 'Cash';
    const CHEQUE = 'Check';
    const CREDIT = 'Credit';
    const CREDITCARD = 'Manual Credit Card';
    const DPS = 'DPS Credit Card Payment';
    const EFTPOS = 'Eftpos';
    const GEOPAY = 'GeoPay';
    const MASTERCARD = 'MASTERCARD';
    const MISC = 'payment';
    const PAYPAL = 'PayPal';
    const ST_GEORGE = 'St.George Credit Card Payment';
}
