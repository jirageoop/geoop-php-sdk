<?php
/**
 * Created by PhpStorm.
 * User: Kieran
 * Date: 25/03/2015
 * Time: 9:42 AM
 */

namespace Geoop\Definitions;

class Endpoints
{
    const ACCOUNT = 'TaxCode';
    const ACCOUNT_NEW = 'Account';
    const BANK_ACCOUNT = 'BankAccount';
    const BILLABLE = 'Billable';
    const COMPANY = 'Account';
    const COMPANY_NEW = 'Company';
    const CUSTOMER = 'Customer';
    const DOCUMENT = 'Document';
    const INVENTORY = 'Inventory';
    const INVOICE = 'Invoice';
    const JOB = 'Job';
    const LABOUR = 'Labour';
    const LINE_ITEM = 'LineItem';
    const PAYMENT = 'Payment';
    const QUOTE = 'Bill';
    const QUOTE_NEW = 'Quote';
    const TAX_RATE = 'TaxRate';
}
