<?php
/**
 * Created by PhpStorm.
 * User: Kieran
 * Date: 25/03/2015
 * Time: 3:13 PM
 */

namespace Geoop\Definitions;

class ServiceEndpoints
{
    const GENERATE_BILL = 'generateBill';
    const GENERATE_INVOICE = 'generateInvoice';
}
