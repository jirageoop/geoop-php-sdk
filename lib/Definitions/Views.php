<?php
/**
 * @author: Kieran <kieran.m@geoop.com>
 * Date: 17-Nov-15
 */

namespace Geoop\Definitions;

class Views
{
    const DEFAULT_VIEW = 'default';
    const EXTERNAL_VIEW = 'external';
    const FULL_VIEW = 'full';
    const MINIMAL_VIEW = 'min';
}
