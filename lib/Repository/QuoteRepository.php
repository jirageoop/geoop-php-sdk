<?php
/**
 * @author: Kieran <kieran.m@geoop.com>
 * Date: 16-Dec-15
 */

namespace Geoop\Repository;

use Geoop\Definitions\Endpoints;
use Geoop\Interfaces\RepositoryInterface;
use Geoop\Model\Quote;
use Geoop\Model\Request;

class QuoteRepository extends BaseRepository implements RepositoryInterface
{
    /**
     * @param Quote[] $quotes
     * @return false|Quote[]
     */
    public function create(array $quotes)
    {
        $request = new Request();
        $request->{Endpoints::QUOTE} = $quotes;

        return $this->post(Endpoints::QUOTE, $request);
    }

    /**
     * @param int $id
     * @return false|Quote
     */
    public function fetchById($id)
    {
        return reset($this->get(Endpoints::QUOTE . '/' . $id, []));
    }

    /**
     * @param array $modifiers
     * @return false|Quote[]
     */
    public function fetch(array $modifiers = [])
    {
        return $this->get(Endpoints::QUOTE, $modifiers);
    }

    /**
     * @param Quote[] $invoices
     * @return false|Quote[]
     */
    public function update(array $invoices)
    {
        $request = new Request();
        $request->{Endpoints::QUOTE} = $invoices;

        return $this->patch(Endpoints::QUOTE, $request);
    }

    /**
     * @param Quote[] $invoices
     * @return false|Quote[]
     */
    public function remove(array $invoices)
    {
        $request = new Request();
        $request->{Endpoints::QUOTE} = $invoices;

        return $this->delete(Endpoints::QUOTE, $request);
    }

    /**
     * @param Quote[] $invoices
     * @return false|Quote[]
     */
    public function replace(array $invoices)
    {
        $request = new Request();
        $request->{Endpoints::QUOTE} = $invoices;

        return $this->put(Endpoints::QUOTE, $request);
    }

    /**
     * @param int $id
     * @return false|string
     */
    public function fetchPdf($id)
    {
        return $this->getFile(Endpoints::QUOTE . '/' . $id . '/pdf');
    }

    /**
     * @param int $id
     * @param string $binary
     * @return bool
     */
    public function replacePdf($id, $binary)
    {
        return $this->putFile(Endpoints::QUOTE . '/' . $id . '/pdf', $binary);
    }
}
