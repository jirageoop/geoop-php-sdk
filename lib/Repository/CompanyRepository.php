<?php
/**
 * @author: Kieran <kieran.m@geoop.com>
 * Date: 22-Sep-15
 */

namespace Geoop\Repository;

use Geoop\Definitions\Endpoints;
use Geoop\Model\BankAccount;
use Geoop\Model\Company;
use Geoop\Model\Request;

class CompanyRepository extends BaseRepository
{
    /**
     * @param array $modifiers
     * @return false|Company
     */
    public function fetch(array $modifiers = [])
    {
        //No endpoint definition required for Company GET
        return reset($this->get(Endpoints::COMPANY, $modifiers));
    }

    /**
     * @param Company $account
     * @return false|Company
     */
    public function update(Company $account)
    {
        $request = new Request();
        $request->{Endpoints::COMPANY} = [$account];

        return reset($this->patch(Endpoints::COMPANY, $request));
    }

    /**
     * @param Company $account
     * @return false|Company
     */
    public function replace(Company $account)
    {
        $request = new Request();
        $request->{Endpoints::COMPANY} = [$account];

        return reset($this->put(Endpoints::COMPANY, $request));
    }

    /**
     * @param BankAccount $bankAccount
     * @return false|BankAccount
     */
    public function setBankAccount(BankAccount $bankAccount)
    {
        $request = new Request();
        $request->{Endpoints::BANK_ACCOUNT} = [$bankAccount];

        return reset($this->put(Endpoints::BANK_ACCOUNT, $request));
    }
}
