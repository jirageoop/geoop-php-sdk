<?php
/**
 * @author: Kieran <kieran.m@geoop.com>
 * Date: 09-Nov-15
 */

namespace Geoop\Repository;

use Geoop\Definitions\Endpoints;
use Geoop\Interfaces\RepositoryInterface;
use Geoop\Model\LineItem;
use Geoop\Model\Request;

class LineItemRepository extends BaseRepository implements RepositoryInterface
{
    /**
     * @param LineItem[] $lineItems
     * @return false|LineItem[]
     */
    public function create(array $lineItems)
    {
        $request = new Request();
        $request->{Endpoints::LINE_ITEM} = $lineItems;

        return $this->post(Endpoints::LINE_ITEM, $request);
    }

    /**
     * @param array $modifiers
     * @return false|LineItem[]
     */
    public function fetch(array $modifiers = [])
    {
        return $this->get(Endpoints::LINE_ITEM, $modifiers);
    }

    /**
     * @param int $id
     * @param array $modifiers
     * @return false|LineItem
     */
    public function fetchById($id, $modifiers = [])
    {
        $response = $this->get(Endpoints::LINE_ITEM . '/' . $id, $modifiers);

        return reset($response);
    }

    /**
     * @param LineItem[] $lineItems
     * @return false|LineItem[]
     */
    public function update(array $lineItems)
    {
        $request = new Request();
        $request->{Endpoints::LINE_ITEM} = $lineItems;

        return $this->patch(Endpoints::LINE_ITEM, $request);
    }

    /**
     * @param LineItem[] $lineItems
     * @return false|LineItem[]
     */
    public function remove(array $lineItems)
    {
        $request = new Request();
        $request->{Endpoints::LINE_ITEM} = $lineItems;

        return $this->delete(Endpoints::LINE_ITEM, $request);
    }

    /**
     * @param LineItem[] $lineItems
     * @return false|LineItem[]
     */
    public function replace(array $lineItems)
    {
        $request = new Request();
        $request->{Endpoints::LINE_ITEM} = $lineItems;

        return $this->put(Endpoints::LINE_ITEM, $request);
    }
}
