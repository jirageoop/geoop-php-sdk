<?php
/**
 * @author: Kieran <kieran.m@geoop.com>
 * Date: 22-Sep-15
 */

namespace Geoop\Repository;

use Geoop\Definitions\Endpoints;
use Geoop\Interfaces\RepositoryInterface;
use Geoop\Model\Customer;
use Geoop\Model\Request;

class CustomerRepository extends BaseRepository implements RepositoryInterface
{
    /**
     * @param Customer[] $customers
     * @return false|Customer[]
     */
    public function create(array $customers)
    {
        $request = new Request();
        $request->{Endpoints::CUSTOMER} = $customers;

        return $this->post(Endpoints::CUSTOMER, $request);
    }

    /**
     * @param array $modifiers
     * @return false|Customer[]
     */
    public function fetch(array $modifiers = [])
    {
        return $this->get(Endpoints::CUSTOMER, $modifiers);
    }

    /**
     * @param int $id
     * @return false|Customer
     */
    public function fetchById($id)
    {
        return reset($this->get(Endpoints::CUSTOMER . '/' . $id));
    }

    /**
     * @param Customer[] $customers
     * @return false|Customer[]
     */
    public function update(array $customers)
    {
        $request = new Request();
        $request->{Endpoints::CUSTOMER} = $customers;

        return $this->patch(Endpoints::CUSTOMER, $request);
    }

    /**
     * @param array $customers
     * @return false|Customer[]
     */
    public function remove(array $customers)
    {
        $request = new Request();
        $request->{Endpoints::CUSTOMER} = $customers;

        return $this->delete(Endpoints::CUSTOMER, $request);
    }

    /**
     * @param Customer[] $customers
     * @return false|Customer[]
     */
    public function replace(array $customers)
    {
        $request = new Request();
        $request->{Endpoints::CUSTOMER} = $customers;

        return $this->put(Endpoints::CUSTOMER, $request);
    }
}
