<?php
/**
 * @author: Kieran <kieran.m@geoop.com>
 * Date: 20-Nov-15
 */

namespace Geoop\Repository;

use Geoop\Definitions\Endpoints;
use Geoop\Interfaces\RepositoryInterface;
use Geoop\Model\BankAccount;
use Geoop\Model\Request;

class BankAccountRepository extends BaseRepository implements RepositoryInterface
{

    /**
     * @param BankAccount[] $bankAccounts
     * @return false|BankAccount[]
     */
    public function create(array $bankAccounts)
    {
        $request = new Request();
        $request->{Endpoints::BANK_ACCOUNT} = $bankAccounts;

        return $this->post(Endpoints::BANK_ACCOUNT, $request);
    }

    /**
     * @param array $modifiers
     * @return false|BankAccount[]
     */
    public function fetch(array $modifiers = [])
    {
        return $this->get(Endpoints::BANK_ACCOUNT, $modifiers);
    }

    /**
     * @param int $id
     * @return false|BankAccount
     */
    public function fetchById($id)
    {
        return reset($this->get(Endpoints::BANK_ACCOUNT . '/' . $id));
    }

    /**
     * @param BankAccount[] $bankAccounts
     * @return false|BankAccount[]
     */
    public function update(array $bankAccounts)
    {
        $request = new Request();
        $request->{Endpoints::BANK_ACCOUNT} = $bankAccounts;

        return $this->patch(Endpoints::BANK_ACCOUNT, $request);
    }

    /**
     * @param BankAccount[] $bankAccounts
     * @return false|BankAccount[]
     */
    public function remove(array $bankAccounts)
    {
        $request = new Request();
        $request->{Endpoints::BANK_ACCOUNT} = $bankAccounts;

        return $this->delete(Endpoints::BANK_ACCOUNT, $request);
    }

    /**
     * @param BankAccount[] $bankAccounts
     * @return false|BankAccount[]
     */
    public function replace(array $bankAccounts)
    {
        $request = new Request();
        $request->{Endpoints::BANK_ACCOUNT} = $bankAccounts;

        return $this->put(Endpoints::BANK_ACCOUNT, $request);
    }
}
