<?php
/**
 * @author: Kieran <kieran.m@geoop.com>
 * Date: 15-Dec-15
 */

namespace Geoop\Repository;

use Geoop\Definitions\Endpoints;
use Geoop\Interfaces\RepositoryInterface;
use Geoop\Model\Labour;
use Geoop\Model\Request;

class LabourRepository extends BaseRepository implements RepositoryInterface
{
    /**
     * @param Labour[] $inventory
     * @return false|Labour[]
     */
    public function create(array $inventory)
    {
        $request = new Request();
        $request->{Endpoints::LABOUR} = $inventory;

        return $this->post(Endpoints::LABOUR, $request);
    }

    /**
     * @param array $modifiers
     * @return false|Labour[]
     */
    public function fetch(array $modifiers = [])
    {
        return $this->get(Endpoints::LABOUR, $modifiers);
    }

    /**
     * @param int $id
     * @param array $modifiers
     * @return false|Labour
     */
    public function fetchById($id, $modifiers = [])
    {
        $response = $this->get(Endpoints::LABOUR . '/' . $id, $modifiers);

        return reset($response);
    }

    /**
     * @param Labour[] $inventory
     * @return false|Labour[]
     */
    public function update(array $inventory)
    {
        $request = new Request();
        $request->{Endpoints::LABOUR} = $inventory;

        return $this->patch(Endpoints::LABOUR, $request);
    }

    /**
     * @param Labour[] $inventory
     * @return false|Labour[]
     */
    public function remove(array $inventory)
    {
        $request = new Request();
        $request->{Endpoints::LABOUR} = $inventory;

        return $this->delete(Endpoints::LABOUR, $request);
    }

    /**
     * @param Labour[] $inventory
     * @return false|Labour[]
     */
    public function replace(array $inventory)
    {
        $request = new Request();
        $request->{Endpoints::LABOUR} = $inventory;

        return $this->put(Endpoints::LABOUR, $request);
    }
}
