<?php
/**
 * @author: Kieran <kieran.m@geoop.com>
 * Date: 22-Sep-15
 */

namespace Geoop\Repository;

use Geoop\Definitions\Endpoints;
use Geoop\Interfaces\RepositoryInterface;
use Geoop\Model\Invoice;
use Geoop\Model\Request;

class InvoiceRepository extends BaseRepository implements RepositoryInterface
{
    /**
     * @param Invoice[] $invoices
     * @return false|Invoice[]
     */
    public function create(array $invoices)
    {
        $request = new Request();
        $request->{Endpoints::INVOICE} = $invoices;

        return $this->post(Endpoints::INVOICE, $request);
    }

    /**
     * @param int $id
     * @return false|Invoice
     */
    public function fetchById($id)
    {
        return reset($this->get(Endpoints::INVOICE . '/' . $id, []));
    }

    /**
     * @param array $modifiers
     * @return false|Invoice[]
     */
    public function fetch(array $modifiers = [])
    {
        return $this->get(Endpoints::INVOICE, $modifiers);
    }

    /**
     * @param Invoice[] $invoices
     * @return false|Invoice[]
     */
    public function update(array $invoices)
    {
        $request = new Request();
        $request->{Endpoints::INVOICE} = $invoices;

        return $this->patch(Endpoints::INVOICE, $request);
    }

    /**
     * @param Invoice[] $invoices
     * @return false|Invoice[]
     */
    public function remove(array $invoices)
    {
        $request = new Request();
        $request->{Endpoints::INVOICE} = $invoices;

        return $this->delete(Endpoints::INVOICE, $request);
    }

    /**
     * @param Invoice[] $invoices
     * @return false|Invoice[]
     */
    public function replace(array $invoices)
    {
        $request = new Request();
        $request->{Endpoints::INVOICE} = $invoices;

        return $this->put(Endpoints::INVOICE, $request);
    }

    /**
     * @param int $id
     * @return false|string
     */
    public function fetchPdf($id)
    {
        return $this->getFile(Endpoints::INVOICE . '/' . $id . '/pdf');
    }

    /**
     * @param int $id
     * @param string $binary
     * @return bool
     */
    public function replacePdf($id, $binary)
    {
        return $this->putFile(Endpoints::INVOICE . '/' . $id . '/pdf', $binary);
    }
}
