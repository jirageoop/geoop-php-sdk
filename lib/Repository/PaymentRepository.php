<?php
/**
 * @author: Kieran <kieran.m@geoop.com>
 * Date: 23-Sep-15
 */

namespace Geoop\Repository;

use Geoop\Definitions\Endpoints;
use Geoop\Interfaces\RepositoryInterface;
use Geoop\Model\Payment;
use Geoop\Model\Request;

class PaymentRepository extends BaseRepository implements RepositoryInterface
{
    /**
     * @param Payment[] $payments
     * @return false|Payment[]
     */
    public function create(array $payments)
    {
        $request = new Request();
        $request->{Endpoints::PAYMENT} = $payments;

        return $this->post(Endpoints::PAYMENT, $request);
    }

    /**
     * @param int $id
     * @return false|Payment
     */
    public function fetchById($id)
    {
        return reset($this->get(Endpoints::PAYMENT . '/' . $id));
    }

    /**
     * @param array $modifiers
     * @return false|Payment[]
     */
    public function fetch(array $modifiers = [])
    {
        return $this->get(Endpoints::PAYMENT, $modifiers);
    }

    /**
     * @param Payment[] $payments
     * @return false|Payment[]
     */
    public function update(array $payments)
    {
        $request = new Request();
        $request->{Endpoints::PAYMENT} = $payments;

        return $this->patch(Endpoints::PAYMENT, $request);
    }

    /**
     * @param Payment[] $payments
     * @return false|Payment[]
     */
    public function remove(array $payments)
    {
        $request = new Request();
        $request->{Endpoints::PAYMENT} = $payments;

        return $this->delete(Endpoints::PAYMENT, $request);
    }

    /**
     * @param Payment[] $payments
     * @return false|Payment[]
     */
    public function replace(array $payments)
    {
        $request = new Request();
        $request->{Endpoints::PAYMENT} = $payments;

        return $this->put(Endpoints::PAYMENT, $request);
    }
}
