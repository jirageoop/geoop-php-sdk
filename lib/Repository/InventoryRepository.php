<?php
/**
 * @author: Kieran <kieran.m@geoop.com>
 * Date: 14-Dec-15
 */

namespace Geoop\Repository;

use Geoop\Definitions\Endpoints;
use Geoop\Interfaces\RepositoryInterface;
use Geoop\Model\Inventory;
use Geoop\Model\Request;

class InventoryRepository extends BaseRepository implements RepositoryInterface
{
    /**
     * @param Inventory[] $inventory
     * @return false|Inventory[]
     */
    public function create(array $inventory)
    {
        $request = new Request();
        $request->{Endpoints::INVENTORY} = $inventory;

        return $this->post(Endpoints::INVENTORY, $request);
    }

    /**
     * @param array $modifiers
     * @return false|Inventory[]
     */
    public function fetch(array $modifiers = [])
    {
        return $this->get(Endpoints::INVENTORY, $modifiers);
    }

    /**
     * @param int $id
     * @param array $modifiers
     * @return false|Inventory
     */
    public function fetchById($id, $modifiers = [])
    {
        $response = $this->get(Endpoints::INVENTORY . '/' . $id, $modifiers);

        return reset($response);
    }

    /**
     * @param Inventory[] $inventory
     * @return false|Inventory[]
     */
    public function update(array $inventory)
    {
        $request = new Request();
        $request->{Endpoints::INVENTORY} = $inventory;

        return $this->patch(Endpoints::INVENTORY, $request);
    }

    /**
     * @param Inventory[] $inventory
     * @return false|Inventory[]
     */
    public function remove(array $inventory)
    {
        $request = new Request();
        $request->{Endpoints::INVENTORY} = $inventory;

        return $this->delete(Endpoints::INVENTORY, $request);
    }

    /**
     * @param Inventory[] $inventory
     * @return false|Inventory[]
     */
    public function replace(array $inventory)
    {
        $request = new Request();
        $request->{Endpoints::INVENTORY} = $inventory;

        return $this->put(Endpoints::INVENTORY, $request);
    }
}
