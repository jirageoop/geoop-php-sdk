<?php
/**
 * @author: Kieran <kieran.m@geoop.com>
 * Date: 09-Nov-15
 */

namespace Geoop\Repository;

use Geoop\Definitions\Endpoints;
use Geoop\Interfaces\RepositoryInterface;
use Geoop\Model\Job;
use Geoop\Model\Payment;
use Geoop\Model\Request;

class JobRepository extends BaseRepository implements RepositoryInterface
{
    /**
     * @param Job[] $jobs
     * @return false|Job[]
     */
    public function create(array $jobs)
    {
        $request = new Request();
        $request->{Endpoints::JOB} = $jobs;

        return $this->post(Endpoints::JOB, $request);
    }

    /**
     * @param int $id
     * @return false|Job
     */
    public function fetchById($id)
    {
        return reset($this->get(Endpoints::JOB . '/' . $id));
    }

    /**
     * @param array $modifiers
     * @return false|Job[]
     */
    public function fetch(array $modifiers = [])
    {
        return $this->get(Endpoints::JOB, $modifiers);
    }

    /**
     * @param Job[] $job
     * @return false|Job[]
     */
    public function update(array $job)
    {
        $request = new Request();
        $request->{Endpoints::JOB} = $job;

        return $this->patch(Endpoints::JOB, $request);
    }

    /**
     * @param Job[] $jobs
     * @return false|Job[]
     */
    public function remove(array $jobs)
    {
        $request = new Request();
        $request->{Endpoints::JOB} = $jobs;

        return $this->delete(Endpoints::JOB, $request);
    }

    /**
     * @param Job[] $job
     * @return false|Job[]
     */
    public function replace(array $job)
    {
        $request = new Request();
        $request->{Endpoints::JOB} = $job;

        return $this->put(Endpoints::JOB, $request);
    }

    /*
     * Service Endpoints
     */

    /**
     * @param int $jobId
     * @param Payment[] $payment
     * @return false|Payment[]
     */
    public function addLineItems($jobId, array $payment)
    {
        $request = new Request();
        $request->{Endpoints::PAYMENT} = $payment;

        return $this->post(Endpoints::JOB . '/' . $jobId . '/LineItem', $request);
    }

    /**
     * @param int $jobId
     * @param Payment[] $payment
     * @return false|Payment[]
     */
    public function updateLineItems($jobId, array $payment)
    {
        $request = new Request();
        $request->{Endpoints::PAYMENT} = $payment;

        return $this->patch(Endpoints::JOB . '/' . $jobId . '/LineItem', $request);
    }

    /**
     * @param int $jobId
     * @param Payment[] $payment
     * @return false|Payment[]
     */
    public function removeLineItems($jobId, array $payment)
    {
        $request = new Request();
        $request->{Endpoints::PAYMENT} = $payment;

        return $this->delete(Endpoints::JOB . '/' . $jobId . '/LineItem', $request);
    }

    /**
     * @param int $jobId
     * @param Payment[] $payment
     * @return false|Payment[]
     */
    public function fetchLineItems($jobId, array $payment)
    {
        $request = new Request();
        $request->{Endpoints::PAYMENT} = $payment;

        return $this->get(Endpoints::JOB . '/' . $jobId . '/LineItem', $request);
    }

    /**
     * @param int $jobId
     * @param Payment[] $payment
     * @return false|Payment[]
     */
    public function createPayments($jobId, array $payment)
    {
        $request = new Request();
        $request->{Endpoints::PAYMENT} = $payment;

        return $this->post(Endpoints::JOB . '/' . $jobId . '/' . Endpoints::PAYMENT, $request);
    }

    /**
     * @param int $jobId
     * @param Payment[] $payment
     * @return false|Payment[]
     */
    public function updatePayments($jobId, array $payment)
    {
        $request = new Request();
        $request->{Endpoints::PAYMENT} = $payment;

        return $this->patch(Endpoints::JOB . '/' . $jobId . '/' . Endpoints::PAYMENT, $request);
    }

    /**
     * @param int $jobId
     * @param Payment[] $payment
     * @return false|Payment[]
     */
    public function removePayments($jobId, array $payment)
    {
        $request = new Request();
        $request->{Endpoints::PAYMENT} = $payment;

        return $this->delete(Endpoints::JOB . '/' . $jobId . '/' . Endpoints::PAYMENT, $request);
    }

    /**
     * @param int $jobId
     * @param array $modifiers
     * @return false|Payment[]
     */
    public function fetchPayments($jobId, array $modifiers = [])
    {
        return $this->get(Endpoints::JOB . '/' . $jobId . '/' . Endpoints::PAYMENT, $modifiers);
    }

    /**
     * @param $jobId
     * @param $binary
     * @return bool
     */
    public function createInvoicePDF($jobId, $binary)
    {
        return $this->postFile(Endpoints::JOB . '/' . $jobId . '/generateInvoice', $binary);
    }

    /**
     * @param $jobId
     * @param $binary
     * @return bool
     */
    public function createQuotePDF($jobId, $binary)
    {
        return $this->postFile(Endpoints::JOB . '/' . $jobId . '/generateBill', $binary);
    }
}
