<?php
/**
 * @author: Kieran <kieran.m@geoop.com>
 * Date: 22-Sep-15
 */

namespace Geoop\Repository;

use Geoop\Core\ApiContext;
use Geoop\Core\ApiParser;
use Geoop\Definitions\Http;
use Geoop\Model\Errors;
use Geoop\Model\Meta;
use Geoop\Model\Request;
use Geoop\Model\Response;
use Geoop\ModelService\ModelAbstract;

abstract class BaseRepository
{
	private $shallowResponse = false;

    /**
     * @var ApiContext $context
     */
    private $context;
    /**
     * @var Response $lastResponse
     */
    private $lastResponse;
    /**
     * @var int $pageSize
     */
    private $pageSize = 500;
    /**
     * @var string $view
     */
    private $view = 'default';

    /**
     * @param ApiContext $context
     */
    public function __construct(ApiContext $context)
    {
        $this->context = $context;
    }

    /**
     * @return int
     */
    public function getPageSize()
    {
        return $this->pageSize;
    }

    /**
     * @param int $pageSize
     * @return $this
     */
    public function setPageSize($pageSize)
    {
        $this->pageSize = $pageSize;

        return $this;
    }

	public function getShallowResponse()
	{
		return $this->shallowResponse;
	}

	public function setShallowResponse($shallowResponse)
	{
		$this->shallowResponse = $shallowResponse;
	}

    /**
     * @param ModelAbstract[] $models
     * @return false|ModelAbstract[]
     */
    public function create(array $models)
    {
        return false;
    }

    /**
     * @param array $modifiers
     * @return false|ModelAbstract[]
     */
    public function fetch(array $modifiers = [])
    {
        return false;
    }

    /**
     * @param int $id
     * @return false|ModelAbstract
     */
    public function fetchById($id)
    {
        return false;
    }

    /**
     * @param ModelAbstract[] $models
     * @return false|ModelAbstract[]
     */
    public function update(array $models)
    {
        return false;
    }

    /**
     * @param ModelAbstract[] $models
     * @return false|ModelAbstract[]
     */
    public function remove(array $models)
    {
        return false;
    }

    /**
     * @param ModelAbstract[] $models
     * @return false|ModelAbstract[]
     */
    public function replace(array $models)
    {
        return false;
    }

    /**
     * @return string
     */
    public function getView()
    {
        return $this->view;
    }

    /**
     * @param string $view
     * @return $this
     */
    public function setView($view)
    {
        $this->view = $view;

        return $this;
    }

    /**
     * @return Response
     */
    public function getLastResponse()
    {
        return $this->lastResponse;
    }

    /**
     * @return \Geoop\Model\Warnings[]
     */
    public function getWarnings()
    {
        return $this->lastResponse->getWarnings();
    }

    /**
     * @return \Geoop\Model\Errors[]
     */
    public function getErrors()
    {
        return $this->lastResponse->getErrors();
    }

    /**
     * @return ApiContext
     */
    public function getContext()
    {
        return $this->context;
    }

    /**
     * @param ApiContext $context
     * @return $this
     */
    public function setContext(ApiContext $context)
    {
        $this->context = $context;

        return $this;
    }

    /**
     * @param string $endpoint
     * @param array $modifiers
     * @return false|ModelAbstract[]
     * @throws \Exception
     */
    protected function get($endpoint, array $modifiers = [])
    {
        return $this->getObjectsOrFalse(
            ApiParser::fromJSON('Response', $this->send(Http::FETCH, $endpoint, $modifiers))
        );
    }

    /**
     * @param string $endpoint
     * @return false|string
     * @throws \Exception
     */
    protected function getFile($endpoint)
    {
        return $this->getBinaryOrFalse(
            $this->sendFile(Http::FETCH, $endpoint)
        );
    }

    /**
     * @param string $endpoint
     * @param Request $payload
     * @return false|ModelAbstract[]
     * @throws \Exception
     */
    protected function delete($endpoint, Request $payload = null)
    {
        return $this->sendPagedRequest(Http::DELETE, $endpoint, $payload);
    }

    /**
     * @param string $endpoint
     * @return bool
     */
    protected function deleteFile($endpoint)
    {
        return $this->sendFile(Http::DELETE, $endpoint);
    }

    /**
     * @param string $endpoint
     * @param Request $payload
     * @return false|ModelAbstract[]
     * @throws \Exception
     */
    protected function patch($endpoint, Request $payload = null)
    {
        return $this->sendPagedRequest(Http::UPDATE, $endpoint, $payload);
    }

    /**
     * @param string $endpoint
     * @param Request $payload
     * @return false|ModelAbstract[]
     * @throws \Exception
     */
    protected function post($endpoint, Request $payload = null)
    {
        return $this->sendPagedRequest(Http::CREATE, $endpoint, $payload);
    }

    /**
     * @param string $endpoint
     * @param string $binary
     * @return bool
     */
    protected function postFile($endpoint, $binary)
    {
        return $this->sendFile(Http::CREATE, $endpoint, $binary);
    }

    /**
     * @param string $endpoint
     * @param Request $payload
     * @return false|ModelAbstract[]
     * @throws \Exception
     */
    protected function put($endpoint, Request $payload = null)
    {
        return $this->sendPagedRequest(Http::UPSERT, $endpoint, $payload);
    }

    /**
     * @param string $endpoint
     * @param string $binary
     * @return bool
     */
    protected function putFile($endpoint, $binary)
    {
        return $this->sendFile(Http::UPSERT, $endpoint, $binary);
    }

    /**
     * @param Response $response
     * @return false|ModelAbstract[]
     */
    private function getObjectsOrFalse(Response $response)
    {
        //Save the Response for raw response access
        $this->lastResponse = $response;

        return $response->meta->success ? $response->getObjects() : false;
    }

    /**
     * @param string $response
     * @return false|string
     */
    private function getBinaryOrFalse($response)
    {
        return !empty($response) ? $response : false;
    }

    /**
     * @param string $endpoint
     * @param array $modifiers
     * @return string
     */
    private function generateApiUrl($endpoint, array $modifiers = [])
    {
        $urlParts = [
            $this->context->getUrl(),
            $this->context->getScope(),
            $this->context->getVersion(),
            $this->context->getCompanyId()
        ];

        if ($urlEndpoint = trim($endpoint)) {
            //Optional endpoint defined
            $urlParts[] = $urlEndpoint;
        }

        return $this->parseModifiersToUrl(join('/', $urlParts), $modifiers);
    }

    /**
     * @param string $url
     * @param array $modifiers
     * @return string
     */
    private function parseModifiersToUrl($url, array $modifiers)
    {
        //Do not use PHP's http_build_query due to incorrect handling of arrays
        $vars = [];
        $view = $this->view;

        foreach ($modifiers as $key => $value) {

            if ('view' == $key) {
                $view = $value;
                continue;
            }

            if (is_array($value)) {
                foreach ($value as $item) {
                    $vars[] = urlencode($key) . '[]=' . urlencode($item);
                }
            } else {
                $vars[] = urlencode($key) . '=' . urlencode($value);
            }
        }

        //Always add view modifier
        $vars[] = 'view=' . (!empty($view) ? $view : 'default');

        if (!empty($vars)) {
            //Prevent invalid URL being returned by checking for existing URL variables
            return (parse_url($url, PHP_URL_QUERY))
                ? rtrim($url, '&') . '&' . join('&', $vars)
                : rtrim($url, '?') . '?' . join('&', $vars);
        }

        return $url;
    }

    /**
     * @param string $method
     * @param string $endpoint
     * @param Request $payload
     * @return false|ModelAbstract[]
     * @throws \Exception
     */
    private function sendPagedRequest($method, $endpoint, Request $payload = null)
    {
        $response = [];

        foreach ($payload as $attribute => $value) {
            if (is_array($value)) {
                //Paginate the entity array
                $smallRequest = new Request();
                $pagedEntities = array_chunk($value, $this->pageSize, true);

                //Send the request for each page
                foreach ($pagedEntities as $page => $entities) {
                    $smallRequest->{$attribute} = $entities;
                    if ($array = $this->getObjectsOrFalse(ApiParser::fromJSON('Response', $this->send($method, $endpoint, [], ApiParser::toJSON($smallRequest))))) {
                        if (is_array($array)) {
							if ($this->getShallowResponse()) {
								$response =  $response + array_fill(count($response), count($array), true);
							} else  {
                            	$response = $response + $array;
							}
                        }
                    } else {
                        //Page failed, act as though all failed (even though may not be case)
                        return false;
                    }
                }
            }
        }

        return $response;
    }

    /**
     * @param string $method
     * @param string $endpoint
     * @param array $modifiers
     * @param string|null $json
     * @return string
     * @throws \Exception
     */
    private function send($method, $endpoint, array $modifiers = [], $json = null)
    {
        $url = $this->generateApiUrl($endpoint, $modifiers);

        $header = [
            'Accept: application/json',
            'Authorization: Bearer ' . $this->context->getToken()
        ];

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, 1);

        if (defined('AWS_GEOOP_NAMESPACE_NAME') && AWS_GEOOP_NAMESPACE_NAME == 'local') {
            //Prevent SSL verification for dev environments
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch, CURLOPT_VERBOSE, 1);
        } else {
            curl_setopt($ch, CURLOPT_VERBOSE, 0);
        }

        if (!is_null($json)) {
            if (Http::UPDATE != $method) {
                //Strip JSON of null values
                $json = preg_replace('/,\s*"[^"]+":null|"[^"]+":null,?/', '', $json);
            }
            //Add payload to request
            curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
        }

        $response = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $headerSize = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
        $body = substr($response, $headerSize);
        $bodyJSON = json_decode($body, true);

        if ((200 != $httpCode) || (false == $bodyJSON['meta']['success'])) {
            trigger_error(
                "API REQUEST (HEADER)...\n" . print_r($header, true)
                . "\nAPI REQUEST (URL)...\n" . print_r($url, true)
                . "\nAPI REQUEST (BODY)...\n" . print_r($json, true)
                . "\nAPI RESPONSE (RAW)...\n" . print_r($response, true)
            );
        }

        return $body;
    }

    /**
     * @param string $method
     * @param string $endpoint
     * @param null $binary
     * @return bool|string
     * @throws \Exception
     */
    private function sendFile($method, $endpoint, $binary = null)
    {
        $url = $this->generateApiUrl($endpoint);

        $header = [
            'Accept: */*',
            'Authorization: Bearer ' . $this->context->getToken()
        ];

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        if (!is_null($binary)) {
            //Add payload to request
            curl_setopt($ch, CURLOPT_POSTFIELDS, $binary);
        }

        $response = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $headerSize = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
        $body = substr($response, $headerSize);

        if (200 != $httpCode) {

            $error = new Errors();
            $error->code = $httpCode;
            $error->message = curl_error($ch);

            $this->lastResponse = new Response();
            $this->lastResponse->meta = new Meta();
            $this->lastResponse->meta->success = false;
            $this->lastResponse->meta->errors = [$error];

            trigger_error(
                "API REQUEST (HEADER)...\n" . print_r($header, true)
                . "\nAPI REQUEST (URL)...\n" . print_r($url, true)
                . "\nAPI REQUEST (BODY)...\n" . print_r(substr($binary, 0, 20), true)
                . "\nAPI RESPONSE (RAW)...\n" . print_r($response, true)
            );
        } else {
            $this->lastResponse = new Response();
            $this->lastResponse->meta = new Meta();
            $this->lastResponse->meta->success = true;
        }

        //Return boolean if CREATE/UPDATE/DELETE, else return binary
        return Http::FETCH == $method ? $body : $this->lastResponse->meta->success;
    }
}
