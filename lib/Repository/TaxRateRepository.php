<?php
/**
 * @author: Kieran <kieran.m@geoop.com>
 * Date: 14-Dec-15
 */

namespace Geoop\Repository;

use Geoop\Definitions\Endpoints;
use Geoop\Interfaces\RepositoryInterface;
use Geoop\Model\Request;
use Geoop\Model\TaxRate;

class TaxRateRepository extends BaseRepository implements RepositoryInterface
{
    /**
     * @param TaxRate[] $taxRates
     * @return false|TaxRate[]
     */
    public function create(array $taxRates)
    {
        $request = new Request();
        $request->{Endpoints::TAX_RATE} = $taxRates;

        return $this->post(Endpoints::TAX_RATE, $request);
    }

    /**
     * @param array $modifiers
     * @return false|TaxRate[]
     */
    public function fetch(array $modifiers = [])
    {
        return $this->get(Endpoints::TAX_RATE, $modifiers);
    }

    /**
     * @param int $id
     * @param array $modifiers
     * @return false|TaxRate
     */
    public function fetchById($id, $modifiers = [])
    {
        $response = $this->get(Endpoints::TAX_RATE . '/' . $id, $modifiers);

        return reset($response);
    }

    /**
     * @param TaxRate[] $taxRates
     * @return false|TaxRate[]
     */
    public function update(array $taxRates)
    {
        $request = new Request();
        $request->{Endpoints::TAX_RATE} = $taxRates;

        return $this->patch(Endpoints::TAX_RATE, $request);
    }

    /**
     * @param TaxRate[] $taxRates
     * @return false|TaxRate[]
     */
    public function remove(array $taxRates)
    {
        $request = new Request();
        $request->{Endpoints::TAX_RATE} = $taxRates;

        return $this->delete(Endpoints::TAX_RATE, $request);
    }

    /**
     * @param TaxRate[] $taxRates
     * @return false|TaxRate[]
     */
    public function replace(array $taxRates)
    {
        $request = new Request();
        $request->{Endpoints::TAX_RATE} = $taxRates;

        return $this->put(Endpoints::TAX_RATE, $request);
    }
}
