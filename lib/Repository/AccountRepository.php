<?php
/**
 * @author: Kieran <kieran.m@geoop.com>
 * Date: 14-Dec-15
 */

namespace Geoop\Repository;

use Geoop\Definitions\Endpoints;
use Geoop\Interfaces\RepositoryInterface;
use Geoop\Model\Account;
use Geoop\Model\Request;

class AccountRepository extends BaseRepository implements RepositoryInterface
{
    /**
     * @param Account[] $accounts
     * @return false|Account[]
     */
    public function create(array $accounts)
    {
        $request = new Request();
        $request->{Endpoints::ACCOUNT} = $accounts;

        return $this->post(Endpoints::ACCOUNT, $request);
    }

    /**
     * @param array $modifiers
     * @return false|Account[]
     */
    public function fetch(array $modifiers = [])
    {
        return $this->get(Endpoints::ACCOUNT, $modifiers);
    }

    /**
     * @param int $id
     * @param array $modifiers
     * @return false|Account
     */
    public function fetchById($id, $modifiers = [])
    {
        $response = $this->get(Endpoints::ACCOUNT . '/' . $id, $modifiers);

        return reset($response);
    }

    /**
     * @param Account[] $accounts
     * @return false|Account[]
     */
    public function update(array $accounts)
    {
        $request = new Request();
        $request->{Endpoints::ACCOUNT} = $accounts;

        return $this->patch(Endpoints::ACCOUNT, $request);
    }

    /**
     * @param Account[] $accounts
     * @return false|Account[]
     */
    public function remove(array $accounts)
    {
        $request = new Request();
        $request->{Endpoints::ACCOUNT} = $accounts;

        return $this->delete(Endpoints::ACCOUNT, $request);
    }

    /**
     * @param Account[] $accounts
     * @return false|Account[]
     */
    public function replace(array $accounts)
    {
        $request = new Request();
        $request->{Endpoints::ACCOUNT} = $accounts;

        return $this->put(Endpoints::ACCOUNT, $request);
    }
}
