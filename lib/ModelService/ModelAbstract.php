<?php
/**
 * All models should be inheriting this class. Place utility functions requiring $this context within
 *
 * User: Kieran
 * Date: 26/03/2015
 * Time: 10:52 AM
 */

namespace Geoop\ModelService;

use Geoop\Interfaces\ModelInterface;

abstract class ModelAbstract implements ModelInterface
{
    /**
     * ModelAbstract constructor.
     * @param bool $sparse
     */
    public function __construct($sparse = false)
    {
        if ($sparse) {
            foreach ($this as $key => &$value) {
                //Some models may have defined default values; keep for now
                if (is_null($this->{$key}) || (is_array($this->{$key}) && empty($this->{$key}))) {
                    unset($this->{$key});
                }
            }
        }
    }

    /**
     * Mass setter from array...if someone is so inclined
     *
     * @param array $keyValues
     */
    public function init($keyValues = [])
    {
        foreach ($keyValues as $key => $value) {
            if (property_exists($this, $key)) {
                $this->{$key} = $value;
            }
        }
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $objectArray = [];

        foreach (get_object_vars($this) as $property => $value) {
            if (!is_object($value)) {
                $objectArray[$property] = $value;
            } else {
                if (in_array('Geoop\\ModelService\\ModelAbstract', class_parents($value))) {
                    //Complex object cast
                    /**
                     * @var ModelAbstract $value
                     */
                    $objectArray[$property] = $value->toArray();
                } else {
                    //Simple object cast
                    $objectArray[$property] = (array)$value;
                }
            }
        }

        return $objectArray;
    }
}
