<?php
/**
 * Created by PhpStorm.
 *
 * @author: Kieran McKewen
 *        Date: 25/02/2015
 *        Time: 9:05 AM
 */

namespace Geoop\ModelService;

abstract class EndpointEntitySettings extends EndpointEntity
{
    /**
     * @var array $settings
     */
    public $settings = [];
}
