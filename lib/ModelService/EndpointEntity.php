<?php
/**
 * Created by PhpStorm.
 *
 * @author: Kieran McKewen
 *        Date: 18/02/2015
 *        Time: 1:56 PM
 */

namespace Geoop\ModelService;

abstract class EndpointEntity extends ModelAbstract
{
    /**
     * @var int $id
     */
    public $id;

    /**
     * @var \Geoop\Model\MetaData $metaData
     */
    public $metaData;
}
