<?php
/**
 * Created by PhpStorm.
 *
 * @author: Kieran McKewen
 *        Date: 20/02/2015
 *        Time: 9:25 AM
 */

namespace Geoop\Model;

class Inventory extends Billable
{
    /**
     * @var string $lastUsed
     */
    public $lastUsed;

    /**
     * @var string $supplier
     */
    public $supplier;

    /**
     * @var AssetAccount $assetAccount
     */
    public $assetAccount;
}
