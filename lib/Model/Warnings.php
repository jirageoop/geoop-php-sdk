<?php
/**
 * Created by PhpStorm.
 * User: Kieran
 * Date: 25/03/2015
 * Time: 9:19 AM
 */

namespace Geoop\Model;

class Warnings
{
    /**
     * @var string $code
     */
    public $code;

    /**
     * @var string $message
     */
    public $message;

    public $context;
}
