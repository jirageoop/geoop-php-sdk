<?php
/**
 * @author: Kieran <kieran.m@geoop.com>
 * Date: 8/06/2015
 */

namespace Geoop\Model;

class InvoiceData
{
    /**
     * @var float $taxTotal
     */
    public $taxTotal;

    /**
     * @var float $amountTotal
     */
    public $amountTotal;

    /**
     * @var float $paymentTotal
     */
    public $paymentTotal;
}
