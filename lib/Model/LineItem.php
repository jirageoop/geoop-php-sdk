<?php
/**
 * Created by PhpStorm.
 *
 * @author: Kieran McKewen
 *        Date: 20/02/2015
 *        Time: 2:39 PM
 */

namespace Geoop\Model;

class LineItem extends DocumentLine
{
    const TYPE_PART = 'part';
    const TYPE_PAYMENT = 'payment';
    const TYPE_TIME = 'time';
    /**
     * @var Quote $quote
     */
    public $quote;

    /**
     * @var Billable $billable
     */
    public $billable;

    /**
     * @var string $code
     */
    public $code;

    /**
     * @var string $description
     */
    public $description;

    /**
     * @var string $endTime
     */
    public $endTime;

    /**
     * @var bool $isActual
     */
    public $isActual;

    /**
     * @var Job $job
     */
    public $job;

    /**
     * @var float $quantity
     */
    public $quantity;

    /**
     * @var string $startTime
     */
    public $startTime;

    /**
     * @var bool $taxable
     */
    public $taxable;

    /**
     * @var PurchaseAccount $purchaseAccount
     */
    public $purchaseAccount;

    /**
     * @var SaleAccount $saleAccount
     */
    public $saleAccount;

    /**
     * @var boolean $toInvoice
     */
    public $toInvoice;

    /**
     * @var string $type
     */
    public $type;

    /**
     * @var float $unitCost
     */
    public $unitCost;

    /**
     * @var float $unitPrice
     */
    public $unitPrice;

    /**
     * @var User $user
     */
    public $user;

    /**
     * @var Visit $visit
     */
    public $visit;
}
