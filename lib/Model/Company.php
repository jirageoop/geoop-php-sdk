<?php
/**
 * Created by PhpStorm.
 *
 * @author: Kieran McKewen
 *        Date: 20/02/2015
 *        Time: 9:22 AM
 */

namespace Geoop\Model;

use Geoop\ModelService\EndpointEntitySettings;

class Company extends EndpointEntitySettings
{
    /**
     * @var Address $address
     */
    public $address;

    /**
     * @var BankAccount $bankAccount
     */
    public $bankAccount;

    /**
     * @var string $companyName
     */
    public $companyName;

    /**
     * @var CountryDetail $countryDetail
     */
    public $countryDetail;

    /**
     * @var string $defaultJobDuration
     */
    public $defaultJobDuration;

    /**
     * @var string $defaultJobStart
     */
    public $defaultJobStart;

    /**
     * @var MailingAddress $address
     */
    public $mailingAddress;

    /**
     * @var ExternalInfo $externalInfo
     */
    public $externalInfo;

    /**
     * @var AssetAccount $assetAccount
     */
    public $assetAccount;

    /**
     * @var PurchaseAccount $purchaseAccountLabour
     */
    public $purchaseAccountLabour;

    /**
     * @var SaleAccount $saleAccountLabour
     */
    public $saleAccountLabour;

    /**
     * @var PurchaseAccount $purchaseAccountInventory
     */
    public $purchaseAccountInventory;

    /**
     * @var SaleAccount $saleAccountInventory
     */
    public $saleAccountInventory;
	
	/**
     * @var SaleAccount $defaultCustomerTax
     */
	public $defaultCustomerTax;

    /**
     * @var IncomeAccount $incomeAccount
     */
    public $incomeAccount;

    /**
     * @var string $timezone
     */
    public $timezone;
}
