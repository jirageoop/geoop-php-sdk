<?php
/**
 * Created by PhpStorm.
 *
 * @author: Kieran McKewen
 *        Date: 27/02/2015
 *        Time: 2:58 PM
 */

namespace Geoop\Model;

class Quote extends FinancialDocument
{
    /**
     * @var Invoice $invoice
     */
    public $invoice;

    /**
     * @var Job $job
     */
    public $job;
}
