<?php
/**
 * @author: Kieran <kieran.m@geoop.com>
 * Date: 3/08/2015
 */

namespace Geoop\Model;

class Context
{
    /**
     * @var Violations $violations
     */
    public $violations;

    /**
     * @var array $data
     */
    public $data;
}
