<?php
/**
 * Created by PhpStorm.
 *
 * @author: Kieran McKewen
 *        Date: 27/02/2015
 *        Time: 2:32 PM
 */

namespace Geoop\Model;

use Geoop\ModelService\EndpointEntity;

class Account extends EndpointEntity
{
    /**
     * @var bool $active
     */
    public $active;

    /**
     * @var string $code
     */
    public $code;

    /**
     * @var string $description
     */
    public $description;

    /**
     * @var ExternalInfo $externalInfo
     */
    public $externalInfo;

    /**
     * @var string $mode
     */
    public $mode;

    /**
     * @var string $name
     */
    public $name;

    /**
     * @var bool $payable
     */
    public $payable;

    /**
     * @var float $rate
     */
    public $rate;

    /**
     * @var TaxRate[] $tax
     */
    public $taxRate;
}
