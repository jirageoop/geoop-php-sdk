<?php
/**
 * Created by PhpStorm.
 * User: Kieran
 * Date: 25/03/2015
 * Time: 9:19 AM
 */

namespace Geoop\Model;

use Geoop\ModelService\ModelAbstract;

class Meta extends ModelAbstract
{
    /**
     * @var int $page
     */
    public $page;

    /**
     * @var int $pageSize
     */
    public $pageSize;

    /**
     * @var int $numberOfPages
     */
    public $numberOfPages;

    /**
     * @var int $numberOfRecords
     */
    public $numberOfRecords;

    /**
     * @var bool $success
     */
    public $success;

    /**
     * @var int $created
     */
    public $created;

    /**
     * @var int $updated
     */
    public $updated;

    /**
     * @var int $deleted
     */
    public $deleted;

    /**
     * @var Errors[] $error
     */
    public $errors;

    /**
     * @var Warnings[] $warning
     */
    public $warnings;
}
