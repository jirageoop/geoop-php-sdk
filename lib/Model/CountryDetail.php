<?php
/**
 * @author: Kieran <kieran.m@geoop.com>
 * Date: 16-Nov-15
 */

namespace Geoop\Model;


class CountryDetail
{
    /**
     * @var float $code
     */
    public $code;

    /**
     * @var string $codeNameLong
     */
    public $codeNameLong;

    /**
     * @var string $codeNameShort
     */
    public $codeNameShort;

    /**
     * @var string $currency
     */
    public $currency;

    /**
     * @var string $name
     */
    public $name;
}
