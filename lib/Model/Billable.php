<?php
/**
 * Created by PhpStorm.
 *
 * @author: Kieran McKewen
 *        Date: 27/02/2015
 *        Time: 1:45 PM
 */

namespace Geoop\Model;

use Geoop\ModelService\EndpointEntity;

class Billable extends EndpointEntity
{
    /**
     * @var string $code
     */
    public $code;

    /**
     * @var ExternalInfo $externalInfo
     */
    public $externalInfo;

    /**
     * @var string $measurement
     */
    public $measurement;

    /**
     * @var string $customMeasurement
     */
    public $customMeasurement;

    /**
     * @var string $description
     */
    public $description;

    /**
     * @var boolean $taxable
     */
    public $taxable;

    /**
     * @var PurchaseAccount $purchaseAccount
     */
    public $purchaseAccount;

    /**
     * @var SaleAccount $saleAccount
     */
    public $saleAccount;

    /**
     * @var string $unitCost
     */
    public $unitCost;

    /**
     * @var string $unitPrice
     */
    public $unitPrice;

    /**
     * @var string $deleted
     */

     public $deleted;

}
