<?php
/**
 * @author: Kieran McKewen <kieran.m@geoop.com>
 * Date: 6/03/2015
 * Time: 9:22 AM
 */

namespace Geoop\Model;

use Geoop\ModelService\ModelAbstract;

class Response extends ModelAbstract
{
    /**
     * @var Meta $meta
     */
    public $meta;

    public function __construct($keys = [], $sparse = false)
    {
        parent::__construct($keys, $sparse);

        //Always initialise Meta object
        $this->meta = new Meta();
    }

    /**
     * New function
     *
     * @return array
     */
    public function getObjects()
    {
        //Return all entities
        foreach ($this as $attribute => $array) {
            if ('meta' != $attribute && !empty($array)) {
                return $array;
            }
        }

        return [];
    }

    /**
     * Legacy function; from when multiple-model service endpoints were looked at
     * @Todo: remove
     *
     * @param  string $entity
     * @return array
     */
    public function getEntities($entity = null)
    {
        $return = [];

        if (!is_null($entity) && isset($this->{ucfirst($entity)})) {
            //Return specific entity array
            $return = $this->{ucfirst($entity)};
        } else {
            //Return all entities
            foreach ($this as $attribute => $array) {
                if ('meta' != $attribute) {
                    $return[$attribute] = $array;
                }
            }
        }

        return $return;
    }

    /**
     * @return Errors[]
     */
    public function getErrors()
    {
        return $this->meta->errors;
    }

    /**
     * @return Warnings[]
     */
    public function getWarnings()
    {
        return $this->meta->warnings;
    }
}
