<?php
/**
 * Created by PhpStorm.
 *
 * @author: Kieran McKewen
 *        Date: 6/03/2015
 *        Time: 9:19 AM
 */

namespace Geoop\Model;

use Geoop\ModelService\EndpointEntity;

class User extends EndpointEntity
{
    /**
     * @var int $id
     */
    public $id;

    /**
     * @var string $role
     */
    public $role;

    /**
     * @var array $location
     */
    public $location;

    /**
     * @var string $workgroup
     */
    public $workgroup;

    /**
     * @var string $firstName
     */
    public $firstName;

    /**
     * @var string $lastName
     */
    public $lastName;

    /**
     * @var string $companyName
     */
    public $companyName;

    /**
     * @var string $email
     */
    public $email;

    /**
     * @var ContactNumber $phoneNumber
     */
    public $phoneNumber;

    /**
     * @var ContactNumber $mobileNumber
     */
    public $mobileNumber;

    /**
     * @var float $hourlyRate
     */
    public $hourlyRate;

    /**
     * @var bool $active
     */
    public $active;
}
