<?php
/**
 * Created by PhpStorm.
 *
 * @author: Kieran McKewen
 *        Date: 18/02/2015
 *        Time: 1:56 PM
 */

namespace Geoop\Model;

use Geoop\ModelService\EndpointEntitySettings;

class Customer extends EndpointEntitySettings
{

    /**
     * @var Address $address
     */
    public $address;

    /**
     * @var Customer $billingCustomer
     */
    public $billingCustomer;

    /**
     * @var string $businessType
     */
    public $businessType;

    /**
     * @var string $code
     */
    public $code;

    /**
     * @var string $companyName
     */
    public $companyName;

    /**
     * @var CountryDetail $countryDetail
     */
    public $countryDetail;

    /**
     * @var bool $deleted
     */
    public $deleted;

    /**
     * @var string $description
     */
    public $description;

    /**
     * @var string $emailAddress
     */
    public $emailAddress;

    /**
     * @var ExternalInfo $externalInfo
     */
    public $externalInfo;

    /**
     * @var ContactNumber $faxNumber
     */
    public $faxNumber;

    /**
     * @var string $firstName
     */
    public $firstName;

    /**
     * @var string $lastName
     */
    public $lastName;

    /**
     * @var string $leadSource
     */
    public $leadSource;

    /**
     * @var  Address $mailingAddress
     */
    public $mailingAddress;

    /**
     * @var ContactNumber $mobileNumber
     */
    public $mobileNumber;

    /**
     * @var string $notes
     */
    public $notes;

    /**
     * @var ContactNumber $phoneNumber
     */
    public $phoneNumber;

    /**
     * @var Account $account
     */
    public $account;

    /**
     * @var string $url
     */
    public $url;


    /**
     * @var string $syncToken
     */
    public $syncToken;
}
