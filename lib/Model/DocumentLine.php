<?php
/**
 * @author: Kieran <kieran.m@geoop.com>
 * Date: 21-Sep-15
 */

namespace Geoop\Model;

use Geoop\ModelService\EndpointEntity;

class DocumentLine extends EndpointEntity
{
    /**
     * @var ExternalInfo $externalInfo
     */
    public $externalInfo;

    /**
     * @var Invoice $invoice
     */
    public $invoice;

    /**
     * @var Job $job
     */
    public $job;

    /**
     * @var Quote $quote
     */
    public $quote;

    /**
     * @var boolean $deleted
     */
    public $deleted;
}
