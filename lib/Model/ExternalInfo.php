<?php
/**
 * Created by PhpStorm.
 * This class requires updating when new integrations are added.
 * The properties aren't generated dynamically due to coupling
 * with the integration service module, so must be maintained
 * manually.
 *
 * @author: Kieran McKewen
 *        Date: 26/02/2015
 *        Time: 2:22 PM
 */

namespace Geoop\Model;

use Geoop\ModelService\ModelAbstract;

class ExternalInfo extends ModelAbstract
{
    /**
     * @var string $qbo
     */
    public $qbo;
    /**
     * @var string $xero
     */
    public $xero;
    /**
     * @var string $freshbooks
     */
    public $freshbooks;
    /**
     * @var string $myob
     */
    public $myob;
    /**
     * @var string $ppy
     */
    public $ppy;

    /**
     * @param bool $sparse
     */
    public function __construct($sparse = false)
    {
        parent::__construct(true);
    }
}
