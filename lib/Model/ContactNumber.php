<?php
/**
 * Created by PhpStorm.
 *
 * @author: Kieran McKewen
 *        Date: 6/03/2015
 *        Time: 12:38 PM
 */

namespace Geoop\Model;

use Geoop\ModelService\ModelAbstract;

class ContactNumber extends ModelAbstract
{
    /**
     * @var number $areaCode
     */
    public $areaCode;

    /**
     * @var int $combinedNumber
     */
    public $combinedNumber;

    /**
     * @var number $countryCode
     */
    public $countryCode;

    /**
     * @var string $displayNumber
     */
    public $displayNumber;

    /**
     * @var int $number
     */
    public $number;
}
