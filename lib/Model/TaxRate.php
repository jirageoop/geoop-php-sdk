<?php
/**
 * Created by PhpStorm.
 *
 * @author: Kieran McKewen
 *        Date: 20/02/2015
 *        Time: 9:47 AM
 */

namespace Geoop\Model;

use Geoop\ModelService\EndpointEntity;

class TaxRate extends EndpointEntity
{
    /**
     * @var string $code
     */
    public $code;

    /**
     * @var string $mode
     */
    public $mode;

    /**
     * @var string $name
     */
    public $name;

    /**
     * @var float $rate
     */
    public $rate;

    /**
     * @var ExternalInfo $externalInfo
     */
    public $externalInfo;

    /**
     * @var Account $account
     */
    public $account;
}
