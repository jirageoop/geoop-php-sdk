<?php
/**
 * @author: Kieran <kieran.m@geoop.com>
 * Date: 8/06/2015
 */

namespace Geoop\Model;

class CompanyData
{
    /**
     * @var string $companyName
     */
    public $companyName;

    /**
     * @var AssetAccount $assetAccount
     */
    public $assetAccount;

    /**
     * @var PurchaseAccount $purchaseAccountLabour
     */
    public $purchaseAccountLabour;

    /**
     * @var SaleAccount $saleAccountLabour
     */
    public $saleAccountLabour;

    /**
     * @var PurchaseAccount $purchaseAccountInventory
     */
    public $purchaseAccountInventory;

    /**
     * @var SaleAccount $saleAccountInventory
     */
    public $saleAccountInventory;

    /**
     * @var IncomeAccount $incomeAccount
     */
    public $incomeAccount;
}
