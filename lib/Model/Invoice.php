<?php
/**
 * Created by PhpStorm.
 *
 * @author: Kieran McKewen
 *        Date: 24/02/2015
 *        Time: 8:41 AM
 */

namespace Geoop\Model;

class Invoice extends FinancialDocument
{
    /**
     * @var Company $companyData
     */
    public $companyData;

    /**
     * @var string $acknowledged
     */
    public $acknowledgeDate;

    /**
     * @var object $invoiceData
     */
    public $invoiceData;

    /**
     * @var string $issueDate
     */
    public $issueDate;

    /**
     * @var Job $job
     */
    public $job;

    /**
     * @var int $version
     */
    public $version;
}
