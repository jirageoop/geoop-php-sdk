<?php
/**
 * Created by PhpStorm.
 * User: Kieran
 * Date: 20/03/2015
 * Time: 8:32 AM
 */

namespace Geoop\Model;

use Geoop\ModelService\EndpointEntity;

class FinancialDocument extends EndpointEntity
{
    /**
     * @var Account $account
     */
    public $account;

    /**
     * @var string $code
     */
    public $code;

    /**
     * @var Customer $customer
     */
    public $customer;

    /**
     * @var string $description
     */
    public $description;

    /**
     * @var ExternalInfo $externalInfo
     */
    public $externalInfo;

    /**
     * @var string $issued
     */
    public $issued;

    /**
     * @var DocumentLine[] $lineItem
     */
    public $lineItem;
}
