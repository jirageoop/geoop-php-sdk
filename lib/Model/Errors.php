<?php
/**
 * Created by PhpStorm.
 *
 * @author: Kieran McKewen
 *        Date: 6/03/2015
 *        Time: 9:23 AM
 */

namespace Geoop\Model;

class Errors
{
    /**
     * @var string $code
     */
    public $code;

    /**
     * @var string $message
     */
    public $message;

    /**
     * @var Context[] $context
     */
    public $context;
}
