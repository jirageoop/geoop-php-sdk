<?php
/**
 * Defined model properties no longer required with new repository architecture
 *
 * @Todo: remove
 *
 * User: Kieran
 * Date: 25/03/2015
 * Time: 9:16 AM
 */

namespace Geoop\Model;

use Geoop\ModelService\ModelAbstract;

class Request extends ModelAbstract
{
    /**
     * @var Company[] $Account
     */
    public $Company;
    /**
     * @var BankAccount[] $BankAccount
     */
    public $BankAccount;
    /**
     * @var Quote[]
     */
    public $Quote;
    /**
     * @var Customer[] $Customer
     */
    public $Customer;
    /**
     * @var Job[] $Job
     */
    public $Job;
    /**
     * @var Inventory[] $Inventory
     */
    public $Inventory;
    /**
     * @var Invoice[] $Invoice
     */
    public $Invoice;
    /**
     * @var Labour[] $Labour
     */
    public $Labour;
    /**
     * @var LineItem[] $LineItem
     */
    public $LineItem;
    /**
     * @var Payment[] $Payment
     */
    public $Payment;
    /**
     * @var Account[] $Account
     */
    public $Account;
    /**
     * @var TaxRate[] $TaxRate
     */
    public $TaxRate;
    /**
     * @var User[] $User
     */
    public $User;

    /**
     * Request constructor.
     * @param bool $sparse
     */
    public function __construct($sparse = false)
    {
        parent::__construct(true);
    }
}
