<?php
/**
 * Created by PhpStorm.
 *
 * @author: Kieran McKewen
 *        Date: 27/02/2015
 *        Time: 3:18 PM
 */

namespace Geoop\Model;

use Geoop\ModelService\EndpointEntity;

class Job extends EndpointEntity
{
    /**
     * @var Customer $billingCustomer
     */
    public $billingCustomer;

    /**
     * @var Customer $customer
     */
    public $customer;

    /**
     * @var string $description
     */
    public $description;

    /**
     * @var LineItem[] $lineItem
     */
    public $lineItem;

    /**
     * @var Payment[] $payment
     */
    public $payment;

    /**
     * @var string $referenceNumber
     */
    public $referenceNumber;

    /**
     * @var string $title
     */
    public $title;
}
