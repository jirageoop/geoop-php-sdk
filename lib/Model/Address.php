<?php
/**
 * Created by PhpStorm.
 *
 * @author: Kieran McKewen
 *        Date: 19/02/2015
 *        Time: 9:19 AM
 */

namespace Geoop\Model;

use Geoop\ModelService\ModelAbstract;

class Address extends ModelAbstract
{
    /**
     * @var string $line1
     */
    public $line1;

    /**
     * @var string $line2
     */
    public $line2;

    /**
     * @var string $city
     */
    public $city;

    /**
     * @var int $postcode
     */
    public $postcode;

    /**
     * @var float $latitude
     */
    public $latitude;

    /**
     * @var float $longitude
     */
    public $longitude;

    /**
     * @var string $state
     */
    public $state;

    /**
     * @var string $country
     */
    public $country;

    /**
     * @var string $countryCode
     */
    public $countryCode;
}
