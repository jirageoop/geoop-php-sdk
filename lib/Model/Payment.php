<?php
/**
 * @author: Kieran <kieran.m@geoop.com>
 * Date: 21-Sep-15
 */

namespace Geoop\Model;

class Payment extends DocumentLine
{
    /**
     * @var float $amount
     */
    public $amount;

    /**
     * @var string $description
     */
    public $description;

    /**
     * @var string $paymentType
     */
    public $paymentType;

    /**
     * @var string $status
     */
    public $status;

    /**
     * @var Account $account
     */
    public $account;

    /**
     * @var User $user
     */
    public $user;
}
