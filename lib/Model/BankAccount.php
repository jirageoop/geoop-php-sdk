<?php
/**
 * @author: Kieran <kieran.m@geoop.com>
 * Date: 16-Sep-15
 */

namespace Geoop\Model;

use Geoop\ModelService\EndpointEntity;

class BankAccount extends EndpointEntity
{
    /**
     * @var string $accountName
     */
    public $accountName;

    /**
     * @var int $accountNumber
     */
    public $accountNumber;

    /**
     * @var string $accountType
     */
    public $accountType;

    /**
     * @var string $bankCountry
     */
    public $bankCountry;

    /**
     * @var string $bankName
     */
    public $bankName;

    /**
     * @var string $currency
     */
    public $currency;

    /**
     * @var ExternalInfo $externalInfo
     */
    public $externalInfo;

    /**
     * @var int $routingNumber
     */
    public $routingNumber;

    /**
     * @var string $holderType
     */
    public $holderType;
}
