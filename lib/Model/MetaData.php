<?php
/**
 * Created by PhpStorm.
 *
 * @author: Kieran McKewen
 *        Date: 6/03/2015
 *        Time: 9:18 AM
 */

namespace Geoop\Model;

class MetaData
{
    /**
     * @var string $created
     */
    public $created;

    /**
     * @var User $createdBy
     */
    public $createdBy;

    /**
     * @var string $modified
     */
    public $modified;

    /**
     * @var User $modifiedBy
     */
    public $modifiedBy;
}
