<?php
/**
 * Legacy class for older implementations
 *
 * User: Kieran
 * Date: 24/03/2015
 * Time: 9:58 AM
 */

namespace Geoop\Core;

use Geoop\Model\Request;
use Geoop\Model\Response;

class GeoOpConnection
{
    const ERROR_401_MESSAGE = 'There was a problem authenticating; please try again later or contact GeoOp support.';
    const ERROR_500_MESSAGE = 'There was a problem processing your request; please contact GeoOp support.';
    const ERROR_DEFAULT_MESSAGE = 'There was a problem processing your request; please contact GeoOp support.';
    public $endpoint;
    public $batchRequest;
    private $apiContext;

    /**
     * @param ApiContext $apiContext
     */
    public function __construct(ApiContext $apiContext)
    {
        $this->apiContext = $apiContext;
    }

    /**
     * @param string $endpoint
     * @param array $fields
     * @return Response
     */
    public function fetch($endpoint, $fields = ['id'])
    {
        $response = $this->send($endpoint, $fields);

        return $response;
    }

    /**
     * @param string $endpoint
     * @param string $method
     * @param Request $payload
     * @param array $modifiers
     * @param bool $sparse
     * @return Response
     * @throws \Exception
     */
    public function send($endpoint, $method, Request $payload = null, $modifiers = null, $sparse = false)
    {
        $requestId = uniqid();

        $header = [
            'Accept: application/json',
            'Authorization: Bearer ' . $this->apiContext->getToken(),
            'X-Request-ID: ' . $requestId
        ];

        $url = $this->generateEndpointUrl($endpoint);

        if (!is_null($modifiers)) {
            $temp = [];
            foreach ($modifiers as $key => $value) {
                $temp[] = $key . '=' . $value;
            }
            //Add request modifiers to the URL
            $url .= '?' . join('&', $temp);
        }

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_VERBOSE, 0);
        curl_setopt($ch, CURLOPT_HEADER, 1);

        if (!is_null($payload)) {
            $reqBody = ApiParser::toJSON($payload);
            if (!is_null($reqBody)) {
                if ($sparse) {
                    //Strip JSON of null values
                    $reqBody = preg_replace('/,\s*"[^"]+":null|"[^"]+":null,?/', '', $reqBody);
                }
                //Add payload to request
                curl_setopt($ch, CURLOPT_POSTFIELDS, $reqBody);
            }
        }

        $response = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $headerSize = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
        $body = substr($response, $headerSize);

        if (500 == $httpCode) {
            throw new \Exception(self::ERROR_500_MESSAGE);
        } elseif (401 == $httpCode) {
            throw new \Exception(self::ERROR_401_MESSAGE);
        } elseif (200 != $httpCode) {
            throw new \Exception(self::ERROR_DEFAULT_MESSAGE);
        }

        return ApiParser::fromJSON('Response', $body);
    }

    /**
     * @param $endpoint
     * @param $payload
     * @return Response
     */
    public function sendFile($endpoint, $payload)
    {
        $header = [
            'Accept: application/json',
            'Authorization: Bearer ' . $this->apiContext->getToken()
        ];

        $url = $this->generateEndpointUrl($endpoint);

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $response = curl_exec($ch);
        $headerSize = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
        $body = substr($response, $headerSize);

        return ApiParser::fromJSON('Response', $body);
    }

    /**
     * @param string $endpoint
     * @return string
     */
    private function generateEndpointUrl($endpoint)
    {
        return join(
            '/',
            [
                $this->apiContext->getUrl(),
                $this->apiContext->getScope(),
                $this->apiContext->getVersion(),
                $this->apiContext->getCompanyId(),
                $endpoint
            ]
        );
    }
}
