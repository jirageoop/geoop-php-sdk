<?php
/**
 * Created by PhpStorm.
 * User: Kieran
 * Date: 27/03/2015
 * Time: 9:55 AM
 */

namespace Geoop\Core;

use Geoop\Model\Request;

class ApiParser
{
    private static $legacyNameMap = [
        'Account' => 'Company',
        'accountData' => 'companyData',
        'TaxCode' => 'Account',
        'Bill' => 'Quote',
        'bill' => 'quote',
        'taxCode' => 'account',
        'taxCodePrice' => 'saleAccount',
        'taxCodeCost' => 'purchaseAccount',
        'taxCodeAsset' => 'assetAccount',
        'taxCodePayment' => 'incomeAccount',
        'taxCodePriceLabour' => 'saleAccountLabour',
        'taxCodeCostLabour' => 'purchaseAccountLabour',
        'taxCodePriceInventory' => 'saleAccountInventory',
        'taxCodeCostInventory' => 'purchaseAccountInventory',
		'defaultCustomerTax' => 'defaultCustomerTax'
	];
    private static $legacyFieldNameMap = [
        'accountData' => 'companyData',
        'bill' => 'quote',
        'taxCode' => 'account',
        'taxCodePrice' => 'saleAccount',
        'taxCodeCost' => 'purchaseAccount',
        'taxCodeAsset' => 'assetAccount',
        'taxCodePayment' => 'incomeAccount',
        'taxCodePriceLabour' => 'saleAccountLabour',
        'taxCodeCostLabour' => 'purchaseAccountLabour',
        'taxCodePriceInventory' => 'saleAccountInventory',
		'taxCodeCostInventory' => 'purchaseAccountInventory',
		'defaultCustomerTax' => 'defaultCustomerTax'
    ];

    /**
     * TODO: Remove name replace logic when APIv3 updated
     *
     * @param Request $input
     * @return string
     */
    public static function toJSON(Request $input)
    {
        //Unset all empty entity arrays
        foreach ($input as $entity => $items) {
            if (is_null($items)) {
                unset($input->{$entity});
                continue;
            }
        }

        //Find and replace
        $input = json_encode($input);

        //Only replace field names (ignore entity names) - strict
        foreach (self::$legacyFieldNameMap as $legacyField => $newField) {
            $input = preg_replace('/\b' . $newField . '\b/', $legacyField, $input);
        }

        return $input;
    }

    /**
     * @param string $entity
     * @param string $input
     * @return mixed
     * @throws \Exception
     */
    public static function fromJSON($entity, $input)
    {
        //Switch old endpoint name to new
        if (in_array($entity, array_keys(self::$legacyNameMap))) {
            $entity = self::$legacyNameMap[$entity];
        }

        $class = '\\Geoop\\Model\\' . $entity;

        if (!class_exists($class)) {
            return json_decode($input, JSON_OBJECT_AS_ARRAY);
        }

        $output = new $class();
        $source = json_decode($input);

        //@todo: Hard-coded for response objects, make dynamic in future
        if ('\\Geoop\\Model\\Response' == $class && !empty($source)) {
            foreach ($source as $entityType => &$objectArray) {
                if ('meta' != $entityType && is_object($objectArray)) {
                    $objectArray = (array)$objectArray;
                }
            }
        }

        if (count($source)) {
            $map = array_flip(self::$legacyNameMap);
            $source = self::convertNames($source, $map);

            foreach ($source as $attribute => $value) {
                if (is_object($value)) {
                    //Sub-object, convert to entity
                    $output->{$attribute} = self::fromObject(ucfirst($attribute), $value);
                } elseif (is_array($value)) {
                    //Convert array contents
                    $output->{$attribute} = self::fromArray(ucfirst($attribute), $value);
                } else {
                    $output->{$attribute} = $value;
                }
            }
        }

        return $output;
    }

    /**
     * @param $entity
     * @param $input
     * @return mixed
     * @throws \Exception
     */
    public static function fromObject($entity, $input)
    {
        $class = '\\Geoop\\Model\\' . ucfirst($entity);

        if (!class_exists($class) || $class === '\\Geoop\\Model\\Context') {
            return json_decode(json_encode($input), true);
        }

        $output = new $class();
        $map = array_flip(self::$legacyNameMap);
        $input = self::convertNames($input, $map);

        foreach ($input as $attribute => $value) {
            if (property_exists($output, $attribute)) {
                if (is_object($value)) {
                    //Sub-object, convert to entity
                    $output->{$attribute} = self::fromObject(ucfirst($attribute), $value);
                } elseif (is_array($value)) {
                    //Convert array contents
                    $output->{$attribute} = self::fromArray(ucfirst($attribute), $value);
                } else {
                    $output->{$attribute} = $value;
                }
            } else {
                if (is_array($value)) {
                    foreach ($value as $key => $item) {
                        if (property_exists($output, $key)) {
                            if (is_object($value)) {
                                //Sub-object, convert to entity
                                $output->{$key}[$attribute] = is_object($value->{$key})
                                    ? self::fromObject(ucfirst($key), $value->{$key})
                                    : self::fromArray(ucfirst($key), $value->{$key});
                            } elseif (is_array($value)) {
                                //Convert array contents
                                $output->{$key}[$attribute] = is_object($value[$key])
                                    ? self::fromObject(ucfirst($key), $value[$key])
                                    : self::fromArray(ucfirst($key), $value[$key]);
                            } else {
                                $output->{$key}[$attribute] = $value;
                            }
                        }
                    }
                }
            }
        }

        return $output;
    }

    /**
     * @param $entity
     * @param $input
     * @return mixed
     * @throws \Exception
     */
    public static function fromArray($entity, $input)
    {
        $class = '\\Geoop\\Model\\' . $entity;

        if (!class_exists($class)) {
            return (array)$input;
        }

        $output = [];

        foreach ($input as $key => $value) {
            if (is_object($value)) {
                $output[$key] = self::fromObject(ucfirst($entity), $value);
            } elseif (is_array($value)) {
                $output[$key] = self::fromArray(ucfirst($entity), $value);
            } else {
                $output[$key] = $value;
            }
        }

        return $output;
    }

    /**
     * @param \stdClass $input
     * @param array $map
     * @return \stdClass
     */
    private static function convertNames($input, $map)
    {
        $inverseMap = array_flip($map);
        $output = new \stdClass();

        foreach ($input as $attribute => $value) {
            if (!is_numeric($attribute) && in_array($attribute, array_values($map))) {
                $output->{$inverseMap[$attribute]} = $value;
                unset($input->{$attribute});
            } else {
                $output->{$attribute} = $value;
            }
        }

        return $output;
    }
}
