<?php
/**
 * Created by PhpStorm.
 * User: Kieran
 * Date: 24/03/2015
 * Time: 10:46 AM
 */

namespace Geoop\Core;

class CallHandler
{
    private $oauth;

    /**
     * @param string $consumerKey
     * @param string $consumerSecret
     */
    public function __construct($consumerKey, $consumerSecret)
    {
        $this->oauth = new \OAuth($consumerKey, $consumerSecret, OAUTH_SIG_METHOD_HMACSHA1);
    }
}
