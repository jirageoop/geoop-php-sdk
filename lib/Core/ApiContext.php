<?php
/**
 * @author: Kieran <kieran.m@geoop.com>
 * Date: 22-Sep-15
 */

namespace Geoop\Core;

class ApiContext
{
    /**
     * @var int $companyId
     */
    public $companyId;

    /**
     * @var string $scope
     */
    public $scope;

    /**
     * @var string $token
     */
    public $token;

    /**
     * @var string $url
     */
    public $url;

    /**
     * @var string $version
     */
    public $version;

    /**
     * ApiContext constructor.
     * @param string $url
     * @param string $scope
     * @param string $version
     * @param int $companyId
     * @param string $token
     */
    public function __construct($url, $scope, $version, $companyId, $token)
    {
        $this->url = $url;
        $this->scope = $scope;
        $this->version = $version;
        $this->companyId = $companyId;
        $this->token = $token;
    }

    /**
     * @return int
     */
    public function getCompanyId()
    {
        return $this->companyId;
    }

    /**
     * @param int $companyId
     * @return ApiContext
     */
    public function setCompanyId($companyId)
    {
        $this->companyId = $companyId;

        return $this;
    }

    /**
     * @return string
     */
    public function getScope()
    {
        return $this->scope;
    }

    /**
     * @param string $scope
     * @return ApiContext
     */
    public function setScope($scope)
    {
        $this->scope = $scope;

        return $this;
    }

    /**
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @param string $token
     * @return ApiContext
     */
    public function setToken($token)
    {
        $this->token = $token;

        return $this;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param string $url
     * @return ApiContext
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * @return string
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * @param string $version
     * @return ApiContext
     */
    public function setVersion($version)
    {
        $this->version = $version;

        return $this;
    }
}
