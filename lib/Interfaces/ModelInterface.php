<?php
/**
 * @author: Kieran <kieran.m@geoop.com>
 * Date: 24-Nov-15
 */

namespace Geoop\Interfaces;

interface ModelInterface
{
    /**
     * @return array
     */
    public function toArray();
}
