<?php
/**
 * @author: Kieran <kieran.m@geoop.com>
 * Date: 24-Nov-15
 */

namespace Geoop\Interfaces;

use Geoop\ModelService\ModelAbstract;

interface SingleItemRepositoryInterface
{
    /**
     * @param ModelAbstract $entity
     * @return false|ModelAbstract
     */
    public function create(ModelAbstract $entity);

    /**
     * @param array $modifiers
     * @return false|ModelAbstract
     */
    public function fetch(array $modifiers = []);

    /**
     * @param ModelAbstract $entity
     * @return false|ModelAbstract
     */
    public function remove(ModelAbstract $entity);

    /**
     * @param ModelAbstract $entity
     * @return false|ModelAbstract
     */
    public function replace(ModelAbstract $entity);

    /**
     * @param ModelAbstract $entity
     * @return false|ModelAbstract
     */
    public function update(ModelAbstract $entity);
}
