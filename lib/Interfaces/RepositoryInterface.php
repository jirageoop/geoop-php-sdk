<?php
/**
 * @author: Kieran <kieran.m@geoop.com>
 * Date: 17-Nov-15
 */

namespace Geoop\Interfaces;

use Geoop\ModelService\ModelAbstract;

interface RepositoryInterface
{
    /**
     * @param ModelAbstract[] $entities
     * @return false|ModelAbstract[]
     */
    public function create(array $entities);

    /**
     * @param array $modifiers
     * @return false|ModelAbstract[]
     */
    public function fetch(array $modifiers = []);

    /**
     * @param int $id
     * @return false|ModelAbstract
     */
    public function fetchById($id);

    /**
     * @param ModelAbstract[] $entities
     * @return false|ModelAbstract[]
     */
    public function update(array $entities);

    /**
     * @param ModelAbstract[] $entities
     * @return false|ModelAbstract[]
     */
    public function remove(array $entities);

    /**
     * @param ModelAbstract[] $entities
     * @return false|ModelAbstract[]
     */
    public function replace(array $entities);
}
